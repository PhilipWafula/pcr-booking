# standard imports
import logging
import os
from argparse import ArgumentParser

# external imports
from confini import Config

# local imports
from pcr_booking.db import dsn_from_config
from pcr_booking.db.models.base import SessionBase
from pcr_booking.http.requests import get_request_method, get_request_endpoint
from pcr_booking.http.routes import handle_user_requests, handle_booking_requests, handle_result_requests
from pcr_booking.http.responses import with_content_headers


logging.basicConfig(level=logging.WARNING)
logg = logging.getLogger()

# define default config directory as would be defined in docker
default_config_dir = '/home/philip/work/metropolitan-hospital/pcr-booking/pcr_booking/data/config'

# define args parser
arg_parser = ArgumentParser(description='CLI for handling cic-ussd server applications.')
arg_parser.add_argument('-c', type=str, default=default_config_dir, help='config root to use')
arg_parser.add_argument('-v', help='be verbose', action='store_true')
arg_parser.add_argument('-vv', help='be more verbose', action='store_true')
arg_parser.add_argument('-q', type=str, default='cic-ussd', help='queue name for worker tasks')
arg_parser.add_argument('--env-prefix',
                        default=os.environ.get('CONFINI_ENV_PREFIX'),
                        dest='env_prefix',
                        type=str,
                        help='environment prefix for variables to overwrite configuration')
args = arg_parser.parse_args()

# define log levels
if args.vv:
    logging.getLogger().setLevel(logging.DEBUG)
elif args.v:
    logging.getLogger().setLevel(logging.INFO)


# parse config
config = Config(args.c, env_prefix=args.env_prefix)
config.process()
config.censor('JWT_SECRET', 'AUTHENTICATION')
config.censor('PASSWORD', 'DATABASE')
logg.debug('config loaded from {}:\n{}'.format(args.c, config))

# set up db
data_source_name = dsn_from_config(config)
SessionBase.connect(data_source_name,
                    pool_size=int(config.get('DATABASE_POOL_SIZE')),
                    debug=config.true('DATABASE_DEBUG'))


def application(env, start_response):
    """
    :param env:
    :type env:
    :param start_response:
    :type start_response:
    :return:
    :rtype:
    """

    errors_headers = [('Content-Type', 'text/plain'), ('Content-Length', '0')]
    headers = [('Content-Type', 'text/plain')]

    # create session for the life-time of http request
    session = SessionBase.create_session()

    # handle pre-flight checks
    if get_request_method(env) == 'OPTIONS':
        headers.append(('Access-Control-Allow-Origin', '*'))
        headers.append(('Access-Control-Allow-Methods', 'DELETE, GET, OPTIONS, PATCH, POST, PUT'))
        headers.append(('Access-Control-Allow-Headers', 'DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,'
                                                        'If-Modified-Since,Cache-Control,Content-Type'))
        headers.append(('Access-Control-Max-Age', '1728000'))
        start_response('204 Come Through', headers)
        return []

    # handle user requests
    if get_request_endpoint(env) in ['/api/v1/register', '/api/v1/users']:
        response, message = handle_user_requests(env, session)
        response_bytes, headers = with_content_headers(headers, response)
        start_response(message, headers)

        session.commit()
        session.close()
        return [response_bytes]

    # handle booking requests
    if get_request_endpoint(env) == "/api/v1/bookings":
        response, message = handle_booking_requests(env, session)
        response_bytes, headers = with_content_headers(headers, response)
        headers.append(('Access-Control-Allow-Origin', '*',))
        start_response(message, headers)

        session.commit()
        session.close()
        return [response_bytes]

    # handle result requests
    if get_request_endpoint(env) == "/api/v1/results":
        response, message = handle_result_requests(env, session)
        response_bytes, headers = with_content_headers(headers, response)
        start_response(message, headers)

        session.commit()
        session.close()
        return [response_bytes]
