"""Create result table.

Revision ID: 7428ceca9b01
Revises: 64a062f07f46
Create Date: 2022-01-14 08:40:31.019670

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import UUID



# revision identifiers, used by Alembic.
revision = '7428ceca9b01'
down_revision = '64a062f07f46'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "result",
        sa.Column("id", UUID(), nullable=False),
        sa.Column("created_at", sa.DateTime(), nullable=True),
        sa.Column("updated_at", sa.DateTime(), nullable=True),
        sa.PrimaryKeyConstraint("id"),
        sa.Column("booking_id", UUID(), nullable=False),
        sa.Column("is_covid_positive", sa.Boolean(), nullable=True),
        sa.Column("parent_user_id", UUID(), nullable=False),
        sa.ForeignKeyConstraint(("booking_id",), ["booking.id"], ),
    )


def downgrade():
    op.drop_table("result")
