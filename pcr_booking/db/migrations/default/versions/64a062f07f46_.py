"""Create booking table.

Revision ID: 64a062f07f46
Revises: 76ba03e14152
Create Date: 2022-01-14 07:44:08.604125

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import UUID



# revision identifiers, used by Alembic.
revision = '64a062f07f46'
down_revision = '76ba03e14152'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "booking",
        sa.Column("id", UUID(), nullable=False),
        sa.Column("first_name", sa.String(length=35), nullable=False),
        sa.Column("last_name", sa.String(length=35), nullable=False),
        sa.Column("phone_number", sa.String(length=13), nullable=True),
        sa.Column("gender", sa.String(), nullable=True),
        sa.Column("date_of_birth", sa.Date(), nullable=True),
        sa.Column("passport_number", sa.String(), nullable=True),
        sa.Column("transport_operator", sa.String(), nullable=True),
        sa.Column("departure_country", sa.String(), nullable=True),
        sa.Column("departure_port", sa.String(), nullable=True),
        sa.Column("destination_country", sa.String(), nullable=True),
        sa.Column("flight_number", sa.String(), nullable=True),
        sa.Column("ticket_number", sa.String(), nullable=True),
        sa.Column("seat_number", sa.String(), nullable=True),
        sa.Column("created_at", sa.DateTime(), nullable=True),
        sa.Column("updated_at", sa.DateTime(), nullable=True),
        sa.PrimaryKeyConstraint("id"),
    )


def downgrade():
    op.drop_table("booking")
