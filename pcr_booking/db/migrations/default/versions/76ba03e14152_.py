"""Create blacklisted tokens.

Revision ID: 76ba03e14152
Revises: 3afd255d4139
Create Date: 2022-01-14 00:31:47.803258

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects.postgresql import UUID



# revision identifiers, used by Alembic.
revision = '76ba03e14152'
down_revision = '3afd255d4139'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        "blacklisted_tokens",
        sa.Column("id", UUID(), nullable=False),
        sa.Column("created_at", sa.DateTime(), nullable=True),
        sa.Column("updated_at", sa.DateTime(), nullable=True),
        sa.Column("token", sa.String(length=500), nullable=False),
        sa.Column("blacklisted_on", sa.DateTime(), nullable=False),
        sa.PrimaryKeyConstraint("id"),
        sa.UniqueConstraint("token"),
    )


def downgrade():
    op.drop_table("blacklisted_tokens")
