# standard imports

# external imports
from sqlalchemy import Boolean, Column, ForeignKey
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import relationship
from sqlalchemy.orm.session import Session

# local imports
from pcr_booking.db.models.base import SessionBase
from pcr_booking.db.models.user import User


class Result(SessionBase):
    __tablename__ = 'result'

    def __init__(self, booking_id: UUID, user_id: UUID):
        self.booking_id = booking_id
        self.user_id = user_id

    is_covid_positive = Column(Boolean)
    booking_id = Column(UUID(), ForeignKey('booking.id'))
    booking = relationship("Booking", back_populates="result")
    parent_user_id = Column(UUID(), ForeignKey("user.id"))
    parent_user = relationship("User",
                               primaryjoin=User.id == parent_user_id,
                               lazy=True,
                               uselist=False)

    def serialize(self):
        covid_status = 'Positive' if self.is_covid_positive is True else 'Negative'
        return {
            'covid_status': covid_status,
            'booking_id': self.booking_id,
            'result_posted_by': self.user_id
        }


def create_result(result_data: dict, session: Session):
    """
    :param result_data:
    :type result_data:
    :param session:
    :type session:
    :return:
    :rtype:
    """
    covid_status = result_data.get("covid_status")
    is_covid_positive = covid_status == "positive"
    booking_id = result_data.get("booking_id")
    user_id = result_data.get("user_id")
    result = Result(booking_id, user_id)
    result.is_covid_positive = is_covid_positive
    session = SessionBase.bind_session(session)
    session.add(result)
    session.flush()
    SessionBase.release_session()
    return result





