# standard imports
import logging
import datetime

# external imports
import uuid

from sqlalchemy import Column, DateTime
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.pool import (
    StaticPool,
    QueuePool,
    AssertionPool,
    NullPool,
)

logg = logging.getLogger().getChild(__name__)

Model = declarative_base(name='Model')


class SessionBase(Model):
    """The base object for all SQLAlchemy enabled models. All other models must extend this.
    """
    __abstract__ = True

    created_at = Column(DateTime, default=datetime.datetime.utcnow)
    updated_at = Column(DateTime, default=datetime.datetime.utcnow, onupdate=datetime.datetime.utcnow)

    id = Column(UUID(as_uuid=True), default=uuid.uuid4, primary_key=True)

    engine = None
    """Database connection engine of the running aplication"""
    session_maker = None
    """Factory object responsible for creating sessions from the  connection pool"""
    transactional = True
    """Whether the database backend supports query transactions. Should be explicitly set by initialization code"""
    poolable = True
    """Whether the database backend supports connection pools. Should be explicitly set by initialization code"""
    procedural = True
    """Whether the database backend supports stored procedures"""
    local_sessions = {}
    """Contains dictionary of sessions initiated by db model components"""

    @staticmethod
    def create_session():
        """Creates a new database session.
        """
        return SessionBase.session_maker()

    @staticmethod
    def _set_engine(engine):
        """Sets the database engine static property
        """
        SessionBase.engine = engine
        SessionBase.session_maker = sessionmaker(bind=SessionBase.engine)

    @staticmethod
    def connect(dsn, debug=False, pool_size=16, ):
        """Create new database connection engine and connect to database backend.

        :param dsn: DSN string defining connection.
        :type dsn: str
        :param debug:
        :type debug:
        :param pool_size:
        :type pool_size:
        """
        e = None
        if SessionBase.poolable:
            pool_class = QueuePool
            if pool_size > 1:
                logg.info('db using queue pool')
                engine = create_engine(dsn,
                                       max_overflow=pool_size * 3,
                                       pool_pre_ping=True,
                                       pool_size=pool_size,
                                       pool_recycle=60,
                                       poolclass=pool_class,
                                       echo=debug)
            else:
                if pool_size == 0:
                    pool_class = NullPool
                elif debug:
                    pool_class = AssertionPool
                else:
                    pool_class = StaticPool
                engine = create_engine(dsn,  echo=debug, poolclass=pool_class)
        else:
            logg.info('db connection not "poolable"')
            engine = create_engine(dsn, echo=debug)

        SessionBase._set_engine(engine)

    @staticmethod
    def disconnect():
        """Disconnect from database and free resources.
        """
        SessionBase.engine.dispose()
        SessionBase.engine = None

    @staticmethod
    def bind_session(session=None):
        local_session = session
        if local_session is None:
            local_session = SessionBase.create_session()
            local_session_key = str(id(local_session))
            logg.debug('creating new session {}'.format(local_session_key))
            SessionBase.local_sessions[local_session_key] = local_session
        return local_session

    @staticmethod
    def release_session(session=None):
        session_key = str(id(session))
        if SessionBase.local_sessions.get(session_key) is not None:
            logg.debug('commit and destroy session {}'.format(session_key))
            session.commit()
            session.close()
            del SessionBase.local_sessions[session_key]
