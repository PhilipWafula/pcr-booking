# standard imports
from datetime import datetime

# external imports
from sqlalchemy import Column, DateTime, String

# local imports
from pcr_booking.db.models.base import SessionBase


class BlacklistedToken(SessionBase):
    """
    Create a blacklisted token
    """

    __tablename__ = "blacklisted_token"

    token = Column(String(500), unique=True, nullable=False)
    blacklisted_on = Column(DateTime, nullable=False)

    @staticmethod
    def check_if_blacklisted(token):
        result = BlacklistedToken.query.filter_by(token=str(token)).first()
        return bool(result)

    def __init__(self, token):
        self.token = token
        self.blacklisted_on = datetime.now()

    def __repr__(self):
        return f"<id: token: {self.token}"
