# standard imports

# external imports
from sqlalchemy import Column, DateTime, String
from sqlalchemy.orm import relationship
from sqlalchemy.orm.session import Session

# local imports
from pcr_booking.db.models.base import SessionBase


class Booking(SessionBase):
    __tablename__ = 'booking'

    def __init__(self, seat_number: str):
        self.seat_number = seat_number

    # patient
    first_name = Column(String)
    last_name = Column(String)
    phone_number = Column(String)
    gender = Column(String)
    date_of_birth = Column(DateTime)
    passport_number = Column(String)

    # trip
    transport_operator = Column(String)
    departure_country = Column(String)
    departure_port = Column(String)
    destination_country = Column(String)
    flight_number = Column(String)
    ticket_number = Column(String)
    seat_number = Column(String)

    result = relationship("Result", back_populates="booking")

    def serialize(self):
        return {
            "first_name": self.first_name,
            "last_name": self.last_name,
            "phone_number": self.phone_number,
            "gender": self.gender,
            "date_of_birth": self.date_of_birth,
            "passport_number": self.passport_number,
            "transport_operator": self.transport_operator,
            "departure_country": self.departure_country,
            "departure_port": self.departure_port,
            "destination_country": self.destination_country,
            "flight_number": self.flight_number,
            "ticket_number": self.ticket_number,
            "seat_number": self.seat_number
        }


def create_booking(booking_data: dict, session: Session):
    """
    :param booking_data:
    :type booking_data:
    :param session:
    :type session:
    :return:
    :rtype:
    """
    print(f'BOOKING DATA IS: {booking_data}')
    first_name = booking_data.get("firstName")
    last_name = booking_data.get("lastName")
    phone_number = booking_data.get("phoneNumber")
    gender = booking_data.get("gender")
    date_of_birth = booking_data.get("dob")
    passport_number = booking_data.get("passportNumber")
    transport_operator = booking_data.get("transportOperator")
    departure_country = booking_data.get("departureCountry")
    departure_port = booking_data.get("departurePort")
    destination_country = booking_data.get("destinationCountry")
    flight_number = booking_data.get("flightNumber")
    ticket_number = booking_data.get("ticketNumber")
    seat_number = booking_data.get("seatNumber")

    booking = Booking(seat_number)
    booking.first_name = first_name
    booking.last_name = last_name
    booking.phone_number = phone_number
    booking.gender = gender
    booking.date_of_birth = date_of_birth
    booking.passport_number = passport_number
    booking.transport_operator = transport_operator
    booking.departure_country = departure_country
    booking.departure_port = departure_port
    booking.destination_country = destination_country
    booking.flight_number = flight_number
    booking.ticket_number = ticket_number

    session = SessionBase.bind_session(session)
    session.add(booking)
    session.flush()
    SessionBase.release_session(session)
    return booking
