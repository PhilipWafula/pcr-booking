# standard imports
from datetime import datetime, timedelta

# external imports
import jwt
from sqlalchemy import Column, String
from sqlalchemy.orm import relationship
from sqlalchemy.orm.session import Session

# local imports
from pcr_booking.db.models.base import SessionBase
from pcr_booking.db.models.blacklisted_token import BlacklistedToken
from pcr_booking.encoder import create_password, is_valid_password
from pcr_booking.secrets import Secrets


class User(SessionBase):
    __tablename__ = 'user'

    def __init__(self, email: str):
        """
        :param email:
        :type email:
        """
        self.email = email

    email = Column(String, index=True, nullable=True, unique=True)
    password_hash = Column(String)
    results = relationship("Result", back_populates="parent_user")

    def encode_auth_token(self):
        """
        Generates the authentication token.
        :return: JSON Web Token.
        """
        try:

            payload = {
                "exp": datetime.utcnow() + timedelta(days=7, seconds=0),
                "iat": datetime.utcnow(),
                "id": self.id,
            }

            return jwt.encode(payload, Secrets.jwt_secret, algorithm="HS256")
        except Exception as exception:
            return exception

    @staticmethod
    def decode_auth_token(token, token_type="authentication"):
        """
        Validates the auth token
        :param token_type: defined token type.
        :param  token: JSON Web Token
        :return: integer|string
        """
        try:
            payload = jwt.decode(jwt=token, key=Secrets.jwt_secret, algorithms="HS256")
            is_blacklisted_token = BlacklistedToken.check_if_blacklisted(token=token)

            if is_blacklisted_token:
                return "Token is blacklisted. Please log in again."
            else:
                return payload
        except jwt.ExpiredSignatureError:
            return f"{token_type} Token Signature expired."
        except jwt.InvalidTokenError:
            return f"Invalid {token_type} Token."

    def hash_password(self, password: str):
        """
        :param password:
        :type password:
        :return:
        :rtype:
        """
        self.password_hash = create_password(password)

    @staticmethod
    def validated_password(hashed_password: str, password: str):
        """
        :param hashed_password:
        :type hashed_password:
        :param password:
        :type password:
        :return:
        :rtype:
        """
        return is_valid_password(hashed_password, password)

    def serialize(self):
        return {
            'email': self.email,
            'id': str(self.id)
        }

    def __repr__(self):
        return "User %r" % self.email


def create_user(session: Session, user_data: dict):
    email = user_data.get('email')
    password = user_data.get('password')
    user = User(email=email)
    user.hash_password(password)
    session = SessionBase.bind_session(session)
    session.add(user)
    session.flush()
    SessionBase.release_session(session)
    return user
