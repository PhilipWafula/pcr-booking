# standard imports
import json

# external imports
from sqlalchemy.orm.session import Session

# local imports
from pcr_booking.db.models.booking import create_booking
from pcr_booking.db.models.user import create_user
from pcr_booking.db.models.result import create_result
from pcr_booking.http.requests import get_request_endpoint, get_request_method


def handle_user_requests(env, session: Session):
    """
    :param env:
    :type env:
    :param session:
    :type session:
    :return:
    :rtype:
    """

    if get_request_method(env) == "POST":
        user_data = json.load(env.get('wsgi.input'))
        user = create_user(session, user_data)
        response = {
            'data': user.serialize(),
            'message': 'User successfully created.'
        }
        response = json.dumps(response)
        return response, '200 OK'


def handle_booking_requests(env, session: Session):
    """
    :param env:
    :type env:
    :param session:
    :type session:
    :return:
    :rtype:
    """
    if get_request_method(env) == 'POST':
        booking_data = json.load(env.get('wsgi.input'))
        booking = create_booking(booking_data, session)
        response = {
            'data': booking.serialize(),
            'message': 'Booking successfully created.'
        }
        response = json.dumps(response)
        return response, '200 OK'


def handle_result_requests(env, session: Session):
    if get_request_method(env) == "POST":
        result_data = json.load(env.get('wsgi.input'))
        result = create_result(result_data, session)
        response = {
            'data': result.serialize(),
            'message': 'Result successfully posted.'
        }
        response = json.dumps(response)
        return response, '200 OK'

