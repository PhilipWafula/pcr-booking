# standard imports
from passlib.hash import pbkdf2_sha512

# external imports

# local imports


def create_password(password: str):
    """
    :param password:
    :type password:
    :return:
    :rtype:
    """
    return pbkdf2_sha512.hash(password)


def is_valid_password(hashed_password: str, password: str):
    """
    :param hashed_password:
    :type hashed_password:
    :param password:
    :type password:
    :return:
    :rtype:
    """
    return pbkdf2_sha512.verify(password, hashed_password)

